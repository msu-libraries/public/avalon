# Copyright 2011-2024, The Trustees of Indiana University and Northwestern
#   University.  Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
# 
# You may obtain a copy of the License at
# 
# http://www.apache.org/licenses/LICENSE-2.0
# 
# Unless required by applicable law or agreed to in writing, software distributed
#   under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
#   CONDITIONS OF ANY KIND, either express or implied. See the License for the
#   specific language governing permissions and limitations under the License.
# ---  END LICENSE_HEADER BLOCK  ---

include Devise::OmniAuth::UrlHelpers

class Users::SessionsController < Devise::SessionsController
  def new
    @omniauthable = devise_mapping.omniauthable? && Avalon::Authentication::VisibleProviders.present?
    @providers = Avalon::Authentication::VisibleProviders
    @local_auth_disabled = params[:admin].blank? && Settings&.auth&.disable_local_auth
    @local_auth_requested = params[:email].present?

    if @local_auth_disabled && @providers.one?
      render :omniauth_new, layout: false
    else
      super
    end
  end

  def destroy
    StreamToken.logout! session
    super
    flash[:success] = flash[:notice]
    flash[:notice] = nil
  end
end
